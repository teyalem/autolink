use serde_derive::Deserialize;
use toml;

use std::fs::File;
use std::fs::OpenOptions;
use std::io::Read;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;

use crate::cli::Result;

static CONFIG_FILE: &str = "autolink.toml";
static CONFIG_DEFAULT: &str = "\
subdir = []
ignore = []
#dot = true
#dest = path
";

#[derive(Debug, Deserialize)]
///Config
pub struct Conf {
    ///subdirectories
    pub subdir: Vec<String>,
    ///ignored files
    pub ignore: Vec<String>,
    ///dots
    pub dot: Option<bool>,
    ///preferred destination
    pub dest: Option<PathBuf>,
}

pub fn create_config_file(path: impl AsRef<Path>) -> Result<()> {
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open(&path.config_file_path())?;
    write!(file, "{}", CONFIG_DEFAULT)?;
    Ok(())
}

impl Conf {
    /// Add default values to config
    fn add_default(mut self) -> Self {
        self.ignore.append(&mut vec![
            "autolink.toml".to_string(),
            "README.md".to_string(),
        ]);
        self
    }

    ///Load config from file
    pub fn load_config(src: &PathBuf) -> Conf {
        let mut file = File::open(src.join("autolink.toml"))
            .expect("ERR: No autolink.toml file in source directory.");

        let mut con = String::new();
        file.read_to_string(&mut con)
            .expect("ERR: Cannot read file");

        toml::from_str::<Conf>(&con)
            .unwrap_or_default()
            .add_default()
    }
}

impl Default for Conf {
    fn default() -> Self {
        Conf {
            subdir: vec![],
            ignore: vec![],
            dot: None,
            dest: None,
        }
    }
}

pub trait DotDir {
    fn has_config(&self) -> bool;
    fn config_file_path(&self) -> String;
}

impl<P: AsRef<Path>> DotDir for P {
    fn has_config(&self) -> bool {
        let path = self.config_file_path();
        let file = Path::new(&path);
        file.exists()
    }

    fn config_file_path(&self) -> String {
        format!("{}/{}", self.as_ref().display(), CONFIG_FILE)
    }
}
