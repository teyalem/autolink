use crate::cli::Result;
use crate::conf::Conf;
use crate::util::*;

use std::path::PathBuf;

pub enum LinkLog {
    Link(PathBuf, PathBuf),
    Subdir(PathBuf),
    Ignore(PathBuf),
}

#[derive(Debug, Clone)]
pub struct LinkBuilder {
    from: PathBuf,
    to: PathBuf,
    dot: bool,
    dry_run: bool,
    all: bool,
    logfun: fn(LinkLog),
    ignore: Vec<PathBuf>,
    subdir: Vec<PathBuf>,
}

impl Default for LinkBuilder {
    fn default() -> Self {
        LinkBuilder {
            from: PathBuf::new(),
            to: PathBuf::new(),
            dot: false,
            dry_run: true,
            all: false,
            logfun: |_| (),
            ignore: Vec::new(),
            subdir: Vec::new(),
        }
    }
}

impl From<Conf> for LinkBuilder {
    fn from(conf: Conf) -> Self {
        let to = match conf.dest {
            Some(path) => path
                .expand_home()
                .canonicalize()
                .expect("Failed to parse destination path in config file. Maybe path is wrong?"),
            None => PathBuf::new(),
        };

        LinkBuilder {
            from: PathBuf::new(),
            to,
            dot: conf.dot.unwrap_or(true),
            dry_run: false,
            all: false,
            logfun: |_| (),
            ignore: conf.ignore.into_iter().map(PathBuf::from).collect(),
            subdir: conf.subdir.into_iter().map(PathBuf::from).collect(),
        }
    }
}

impl LinkBuilder {
    pub fn set_source(mut self, s: PathBuf) -> Result<Self> {
        self.from = s.canonicalize()?;
        Ok(self)
    }

    pub fn try_set_dest(mut self, d: PathBuf) -> Result<Self> {
        if self.to == PathBuf::new() {
            self.to = d.canonicalize()?;
        }

        Ok(self)
    }

    pub fn set_dry_run(mut self, d: bool) -> Self {
        self.dry_run = d;
        self
    }

    pub fn set_logger(mut self, f: fn(LinkLog)) -> Self {
        self.logfun = f;
        self
    }

    pub fn set_dot(mut self, dot: bool) -> Self {
        self.dot = dot;
        self
    }

    pub fn set_all(mut self, all: bool) -> Self {
        self.all = all;
        self
    }

    fn is_ignored(&self, file: &PathBuf) -> bool {
        self.ignore.iter().any(|f| file.ends_with(f))
    }

    //Is this path subdirectory?
    fn is_subdir(&self, path: &PathBuf) -> bool {
        self.subdir.iter().any(|f| path.ends_with(f))
    }

    fn get_destination_path_of(&self, path: &PathBuf) -> PathBuf {
        //determine target file name according to self.dot property
        let file_name = if self.dot && !path.is_dotted() {
            path.file_name_with_dot()
        } else {
            path.file_name_as_string()
        };

        self.to.join(file_name)
    }

    //Link subdirectory
    fn link_subdir(&self, dir: PathBuf) -> Result<()> {
        (self.logfun)(LinkLog::Subdir(dir.clone()));

        let mut subself = self.clone();
        subself.from = subself.from.join(&dir);
        subself.to = subself.get_destination_path_of(&dir);

        //disable dotting because parents already have dots.
        subself.set_dot(false).build()
    }

    //Link ignored
    //actually doesn't link, only log
    fn link_ignore(&self, path: PathBuf) -> Result<()> {
        (self.logfun)(LinkLog::Ignore(path));
        Ok(())
    }

    //Link single file
    //depend on self.dot property
    fn link_file(&self, src: PathBuf, dst: PathBuf) -> Result<()> {
        (self.logfun)(LinkLog::Link(src.clone(), dst.clone()));

        if self.dry_run {
            Ok(())
        } else {
            link(&src, &dst)
        }
    }

    //Link
    fn link(&self, src: PathBuf) -> Result<()> {
        if self.is_subdir(&src) {
            self.link_subdir(src)
        } else if self.is_ignored(&src) {
            self.link_ignore(src)
        } else {
            let dst = self.get_destination_path_of(&src);
            self.link_file(src, dst)
        }
    }

    ///Build links
    pub fn build(self) -> Result<()> {
        let files = self.from.read_dir()?;

        files
            .map(|f| f.unwrap().path())
            //include dotted file or not
            .filter(|f| self.all || !f.is_dotted())
            .map(|f| self.link(f))
            //Take Last Err
            //TODO: instead print errors
            .fold(Ok(()), |pre, r| r.or(pre))
    }
}
