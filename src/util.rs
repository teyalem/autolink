use std::env::var;
use std::os::unix::fs;
use std::path::Path;
use std::path::PathBuf;

use crate::cli;

//Get home directory path
pub fn get_home() -> String {
    var("HOME").expect("ERR: Cannot find home directory")
}

//Link file
pub fn link<T: AsRef<Path>>(src: T, dst: T) -> cli::Result<()> {
    fs::symlink(src, dst).map_err(cli::Error::from)
}

pub trait ExPath {
    fn file_name_as_string(&self) -> String;
    fn file_name_with_dot(&self) -> String;
    fn is_dotted(&self) -> bool;
    fn format_path(&self) -> String;
    fn expand_home(&self) -> PathBuf;
}

impl ExPath for Path {
    //Get file_name as String
    fn file_name_as_string(&self) -> String {
        self.file_name().unwrap().to_str().unwrap().to_owned()
    }

    //Return dot-attached file name
    fn file_name_with_dot(&self) -> String {
        format!(".{}", self.file_name_as_string())
    }

    //Is it already dotted?
    fn is_dotted(&self) -> bool {
        self.file_name_as_string().starts_with('.')
    }

    //Make path printing clean
    fn format_path(&self) -> String {
        let pwd: String = var("PWD").unwrap_or_default();
        let pwd = format!("{}/", pwd);
        let home = var("HOME").unwrap_or_default();
        let path = self.to_str().expect("ERR: Not a unicode path");
        path.replace(&pwd, "").replace(&home, "~").to_owned()
    }

    //Expand home sign in path
    fn expand_home(&self) -> PathBuf {
        if self.starts_with("~") {
            let home = get_home();
            PathBuf::from(self.to_str().unwrap().replace("~", &home))
        } else {
            self.to_owned()
        }
    }
}
