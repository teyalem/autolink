#[macro_use]
extern crate clap;

mod cli;
mod cmd;
mod conf;
mod link;
mod util;

use cli::cli;
use cli::Result;
use cmd::{cli_list, return_cmd};

fn main() -> Result<()> {
    let app = cli(cli_list());
    let mut help = app.clone();
    let args = app.get_matches();
    let cmd_name = args.subcommand_name().unwrap_or("");
    if cmd_name == "" {
        help.print_help().unwrap();
        Ok(())
    } else {
        let cmd = return_cmd(cmd_name).unwrap();
        let cmd_args = args.subcommand_matches(cmd_name).unwrap();

        cmd(cmd_args)
    }
}
