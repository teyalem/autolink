use super::*;

use crate::conf::create_config_file;
use crate::conf::DotDir;

pub fn cli() -> App {
    clap_app!(
        init =>
        (about: "Initializes a dotfile directory")
        (@arg dir: "Path of the dotfile directory")
    )
}

pub fn run(args: &ArgMatches) -> Result<()> {
    let path = args.value_of("dir").unwrap_or(".");
    if path.has_config() {
        eprintln!("ERR: Config already exists at that directory.");
        Ok(())
    } else {
        create_config_file(path)
    }
}
