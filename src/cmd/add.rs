use super::*;

use std::fs::rename;
use std::path::Path;
use std::path::PathBuf;

use crate::util::*;

pub fn cli() -> App {
    clap_app!(
        add =>
        (about: "Adds a file to some destination and links that to original location")
        (@arg file: +required "the file to add")
        (@arg destination: "Destination of the file")
    )
}

///This function does 2 things:
///1. move the file to selected dir
///2. link moved file to original path
pub fn run(args: &ArgMatches) -> Result<()> {
    let home = get_home();

    let file = args.value_of("file").unwrap();
    let file = PathBuf::from(file)
        .canonicalize()
        .expect("ERR: Not a valid file");

    let default_dst = home + "/etc";
    let dest = args.value_of("destination").unwrap_or(&default_dst);
    let dest_file = determine_dest_path(dest, &file);

    rename(&file, &dest_file)?;

    link(&dest_file, &file)?;

    Ok(())
}

fn determine_dest_path(dest: impl AsRef<Path>, file: impl AsRef<Path>) -> PathBuf {
    let fname = file.as_ref().file_name_as_string();
    let dotless = fname.trim_start_matches(".");
    dest.as_ref().join(dotless)
}
