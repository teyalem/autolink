use crate::cli::{App, ArgMatches, Result};

pub type Command = fn(&ArgMatches) -> Result<()>;

//command modules
mod add;
mod init;
mod link;
mod new;

//Returns list of cli commands
pub fn cli_list() -> Vec<App> {
    vec![link::cli(), init::cli(), new::cli(), add::cli()]
}

//Provide apropriate command by command string
pub fn return_cmd(cmd: &str) -> Option<Command> {
    let cmd = match cmd {
        "link" => link::run,
        "init" => init::run,
        "new" => new::run,
        "add" => add::run,
        _ => return None,
    };
    Some(cmd)
}
