use std::fs::create_dir;

use super::*;
use crate::conf::create_config_file;

pub fn cli() -> App {
    clap_app!(
        new =>
        (about: "Create a new dotfile directory")
        (@arg dir: "Path of the new dotfile directory")
    )
}

pub fn run(args: &ArgMatches) -> Result<()> {
    let path = args
        .value_of("dir")
        .expect("ERR: need at least one argument");
    create_dir(path)?;
    create_config_file(&path)?;

    Ok(())
}
