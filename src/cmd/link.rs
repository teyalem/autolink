use std::path::PathBuf;

use super::*;
use crate::conf::Conf;
use crate::link::*;
use crate::util::*;

//Logger
fn logger(log: LinkLog) {
    match log {
        LinkLog::Link(from, to) => {
            let from = from.format_path();
            let to = to.format_path();
            eprintln!("link {} to {}", from, to);
        }
        LinkLog::Subdir(s) => {
            let s = s.format_path();
            eprintln!("enter subdirectory {}", s);
        }
        LinkLog::Ignore(i) => {
            let i = i.format_path();
            eprintln!("ignore file {}", i);
        }
    }
}

pub fn cli() -> App {
    clap_app!(
        link =>
        (about: "Links files")
        (@arg source: "Set the source directory")
        (@arg destination: "Set the destination directory")
        (@arg verbose: -v --verbose "Print verbosely")
        (@arg real: -r --real "Really link files")
        (@arg undot: -u --undot "Don't attach dots")
        (@arg all: -a --all "Link all file without regard to dots")
    )
}

pub fn run(args: &ArgMatches) -> Result<()> {
    let home = get_home();

    //default values
    let default_src = home.clone() + "/etc";
    let default_dst = home;

    //Arguments
    let src = args.value_of("source").unwrap_or(&default_src);
    let dst = args.value_of("destination").unwrap_or(&default_dst);
    let real = args.is_present("real");
    let undot = args.is_present("undot");
    let all = args.is_present("all");
    let verbose = args.is_present("verbose");

    //Load per-directory configuration
    let conf = Conf::load_config(&PathBuf::from(src));

    let mut lb = LinkBuilder::from(conf)
        .set_source(PathBuf::from(src))?
        .try_set_dest(PathBuf::from(dst))?
        .set_dot(!undot)
        .set_all(all);

    if verbose {
        lb = lb.set_logger(logger);
    }

    if !real {
        eprintln!("Dry run enabled. If you want to really link dotfiles, use -r flag.");
        lb = lb.set_dry_run(true);
    }

    lb.build()
}
