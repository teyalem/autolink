pub type Result<T> = std::result::Result<T, Error>;
pub type App = clap::App<'static, 'static>;
pub type ArgMatches = clap::ArgMatches<'static>;

#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    Clap(clap::Error),
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::IO(e)
    }
}

impl From<clap::Error> for Error {
    fn from(e: clap::Error) -> Self {
        Error::Clap(e)
    }
}

use std::fmt;
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IO(e) => write!(f, "ERR: {}", e),

            Error::Clap(e) => write!(f, "ERR: {}", e),
        }
    }
}

pub fn cli(cmds: Vec<App>) -> App {
    let app = clap_app!(
        autolink =>
        (version: crate_version!())
        (author: crate_authors!())
        (about: crate_description!())
    );

    app.subcommands(cmds)
}
