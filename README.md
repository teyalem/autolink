Autolink
==========

Link (dot)files smart way

USAGE:
    autolink [FLAGS] [ARGS]

FLAGS:
    -h, --help       Prints help information
    -r, --real       Really links files
    -u, --undot      Does not attach dots
    -V, --version    Prints version information

ARGS:
    <source>         Sets the source directory
    <destination>    Sets the destination directory

Autolink.toml
---------------

- ignore: ignore that file
- subdir: link file in the directory, not touching existing destination directory (useful for .config)
